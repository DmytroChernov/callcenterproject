﻿using CCP.BLL.Services.Services;
using CCP.DAL.Repository;
using CCP.WebSiteApplication.Hubs;
using CCP.WebSiteApplication.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Diagnostics;

namespace CCP.WebSiteApplication.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var model = new SimulationSettingsModels();
            return View(model);
        }

        public IActionResult StartSimulation(SimulationSettingsModels model)
        {
            var parameterService = new ParametersAppService();
            parameterService.SetInitialParameters(model);
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
