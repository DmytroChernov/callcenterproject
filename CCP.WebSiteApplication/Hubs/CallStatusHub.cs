﻿using CCP.BLL.Interfaces;
using CCP.BLL.Interfaces.IModels;
using CCP.BLL.Services.Services;
using CCP.WebSiteApplication.Models;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCP.WebSiteApplication.Hubs
{
    public class CallStatusHub : Hub
    {
        private readonly object locker = new object();
        private IParametersAppService _parametersService;

        private List<EmployeeViewModel> _employees = new List<EmployeeViewModel>();
        private Queue<int> _calls = new Queue<int>();
        public async void InitialEvent()
        {
            _parametersService = new ParametersAppService();
            _parametersService.GetCalls()
                .ToList()
                .ForEach(item => _calls.Enqueue(item));
            _employees = _parametersService.GetEmployees()
                                           .Select(employee => GetEmployee(employee))
                                           .OrderBy(employee => employee.Type)
                                           .ToList();
            await this.Clients.All.SendAsync("InitialApp", _calls, _employees);

            var tasks = new List<Task>();
            foreach (var employee in _employees)
            {
                var t = employee;
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    Worker(employee);
                }));
            }
            Task.WaitAll(tasks.ToArray());
        }

        private void Worker(EmployeeViewModel employee)
        {
            while (_calls.Count() > 0)
            {
                if (!_employees.Any(item => item.Type < employee.Type && item.Status))
                {
                    var call = GetCall();
                    employee.Status = false;
                    this.Clients.All.SendAsync("Send", $"Hello! I’m {employee.Type} ({employee.Id}) ({call}).");
                    if (!_employees.Any(i => i.Status))
                    {
                        this.Clients.All.SendAsync("Send", $"Sorry! All operators are busy. Try again later.");
                    }
                    Do(call);
                    employee.Status = true;
                    this.Clients.All.SendAsync("Send", $"{employee.Type} ({employee.Id}) ended a call({call}).");
                }
            }
        }

        private int GetCall()
        {
            lock (locker)
            {
                if (_calls.Count() == 0)
                {
                    return -1;
                }
                return _calls.Dequeue();
            }
        }

        private void Do(int time)
        {
            Thread.Sleep(time);
        }

        private EmployeeViewModel GetEmployee(IBLLEmployeeModel model)
        {
            return new EmployeeViewModel()
            {
                Id = model.Id,
                Status = model.Status,
                Type = model.Type
            };
        }
    }
}
