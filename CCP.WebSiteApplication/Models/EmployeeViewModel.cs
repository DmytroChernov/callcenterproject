﻿using CCP.BLL.Interfaces.IModels;
using System;

namespace CCP.WebSiteApplication.Models
{
    public class EmployeeViewModel : IBLLEmployeeModel
    {
        public Guid Id { get; set; }
        public int Type { get; set; }
        public bool Status { get; set; }
    }
}
