﻿using CCP.BLL.Interfaces.IModels;

namespace CCP.WebSiteApplication.Models
{
    public class SimulationSettingsModels : IBLLParametersAppModel
    {
        public int OperatorCount { get; set; }
        public int ManagerCount { get; set; }
        public int SeniorManagerCount { get; set; }
        public int IncomingCallsCount { get; set; }
        public int MinDurationCall { get; set; }
        public int MaxDurationCall { get; set; }
    }
}
