﻿using CCP.DAL.Interfaces;
using CCP.DAL.Interfaces.IModels;
using CCP.DAL.Repository.Models;

namespace CCP.DAL.Repository
{
    public class XmlParametersSerializer : BaseXmlSerializer<ParametersAppModel>, IXmlParametersSerializer
    {
        private readonly string _patternPath = $"{nameof(XmlParametersSerializer)}.xml";

        public IDALParametersAppModel Get()
        {
            return base.GetValue(_patternPath);
        }

        public void Set(IDALParametersAppModel model)
        {
            var entity = new ParametersAppModel()
            {
                MaxDurationCall = model.MaxDurationCall,
                MinDurationCall = model.MinDurationCall,
                IncomingCallsCount = model.IncomingCallsCount,
                ManagerCount = model.ManagerCount,
                OperatorCount = model.OperatorCount,
                SeniorManagerCount = model.SeniorManagerCount
            };
            base.Set(_patternPath, entity);
        }
    }
}