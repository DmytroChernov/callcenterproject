﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace CCP.DAL.Repository
{
    public abstract class BaseXmlSerializer<T>
    {
        protected List<T> Get(string path)
        {
            var results = new List<T>();
            var serializer = new XmlSerializer(typeof(List<T>));
            using (var stream = File.OpenRead(path))
            {
                var other = (List<T>)(serializer.Deserialize(stream));
                results.AddRange(other);
            }
            return results;
        }
        protected T GetValue(string path)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(T));

            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                var result = (T)formatter.Deserialize(fs);
                return result;
            }
        }

        protected void Set(string path, List<T> model)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<T>));

            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs, model);
            }
        }
        protected void Set(string path, T model)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(T));

            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs, model);
            }
        }
    }
}
