﻿using CCP.DAL.Interfaces.Models;
using System;

namespace CCP.DAL.Repository.Models
{
    [Serializable]
    public class EmployeeModel : IDALEmployeeModel
    {
        public Guid Id { get; set; }

        public int Type { get; set; }

        public bool Status { get; set; }
    }
}
