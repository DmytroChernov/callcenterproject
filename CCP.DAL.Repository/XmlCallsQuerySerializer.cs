﻿using CCP.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CCP.DAL.Repository
{
    public class XmlCallsQuerySerializer : BaseXmlSerializer<int>, IXmlCallsQuerySerializer
    {
        private readonly string _patternPath = $"{nameof(XmlCallsQuerySerializer)}.xml";

        public IEnumerable<int> Get()
        {
            return base.Get(_patternPath);
        }

        public void Set(IEnumerable<int> model)
        {
            base.Set(_patternPath, model.ToList());
        }
    }
}
