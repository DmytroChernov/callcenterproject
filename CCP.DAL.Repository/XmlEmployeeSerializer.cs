﻿using System.Collections.Generic;
using System.Linq;
using CCP.DAL.Interfaces;
using CCP.DAL.Interfaces.Models;
using CCP.DAL.Repository.Models;

namespace CCP.DAL.Repository
{
    public class XmlEmployeeSerializer : BaseXmlSerializer<EmployeeModel>, IXmlEmployeeSerializer
    {
        private readonly string _patternPath = $"{nameof(XmlEmployeeSerializer)}.xml";

        public IEnumerable<IDALEmployeeModel> Get()
        {
            return base.Get(_patternPath);
        }

        public void Set(IEnumerable<IDALEmployeeModel> model)
        {
            var entities = model.Select(item => new EmployeeModel()
            {
                Id = item.Id,
                Status = item.Status,
                Type = item.Type
            });
            base.Set(_patternPath, entities.ToList());
        }
    }
}
