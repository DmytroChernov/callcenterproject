﻿namespace CCP.BLL.Services.Enums
{
    public enum EmployeeTypes
    {
        Operator,
        Manager,
        SeniorManager
    }
}
