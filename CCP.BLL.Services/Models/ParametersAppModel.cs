﻿using CCP.BLL.Interfaces.IModels;
using CCP.DAL.Interfaces.IModels;

namespace CCP.BLL.Services.Models
{
    public class ParametersAppModel : IBLLParametersAppModel, IDALParametersAppModel
    {
        public int OperatorCount { get; set; }
        public int ManagerCount { get; set; }
        public int SeniorManagerCount { get; set; }
        public int IncomingCallsCount { get; set; }
        public int MinDurationCall { get; set; }
        public int MaxDurationCall { get; set; }
    }
}
