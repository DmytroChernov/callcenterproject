﻿using CCP.BLL.Interfaces.IModels;
using CCP.DAL.Interfaces.Models;
using System;

namespace CCP.BLL.Services.Models
{
    public class BaseEmployee : IBLLEmployeeModel, IDALEmployeeModel
    {
        public Guid Id { get; set; }
        public int Type { get; set; }
        public bool Status { get; set; }
    }
}
