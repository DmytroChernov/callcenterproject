﻿using CCP.BLL.Interfaces;
using CCP.BLL.Interfaces.IModels;
using CCP.BLL.Services.Enums;
using CCP.BLL.Services.Models;
using CCP.DAL.Interfaces;
using CCP.DAL.Interfaces.IModels;
using CCP.DAL.Interfaces.Models;
using CCP.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CCP.BLL.Services.Services
{
    public class ParametersAppService : IParametersAppService
    {
        private readonly IXmlCallsQuerySerializer _callsRepository;
        private readonly IXmlEmployeeSerializer _employeeRepository;
        private readonly IXmlParametersSerializer _parametersRepository;

        public ParametersAppService()
        {
            _callsRepository = new XmlCallsQuerySerializer();
            _employeeRepository = new XmlEmployeeSerializer();
            _parametersRepository = new XmlParametersSerializer();
        }

        public void SetInitialParameters(IBLLParametersAppModel model)
        {
            var ListOfCalls = GetCallsQuery(model.IncomingCallsCount, model.MinDurationCall, model.MaxDurationCall);
            var ListOfEmployees = GetAllEmployees(model.OperatorCount, model.ManagerCount, model.SeniorManagerCount);

            _callsRepository.Set(ListOfCalls);
            _employeeRepository.Set(ListOfEmployees);
            _parametersRepository.Set(GetDalModel(model));
        }

        public IEnumerable<IBLLEmployeeModel> GetEmployees()
        {
            var results =  _employeeRepository.Get();
            return results.Select(employee => new BaseEmployee()
            {
                Id = employee.Id,
                Status = employee.Status,
                Type = employee.Type
            });
        }

        public IEnumerable<int> GetCalls()
        {
            return _callsRepository.Get();
        }


        private List<int> GetCallsQuery(int countCalls, int minDuration, int maxDuration)
        {
            var calls = new List<int>();
            for (int index = 0; index < countCalls; index++)
            {
                Random rnd = new Random();
                int duration = rnd.Next(minDuration * 1000, maxDuration * 1000);
                calls.Add(duration);
            }
            return calls;
        }

        private List<IDALEmployeeModel> GetAllEmployees(int operators, int managers, int seniors)
        {
            var results = new List<IDALEmployeeModel>();
            results.AddRange(GetAllEmployeeByType(operators, EmployeeTypes.Operator));
            results.AddRange(GetAllEmployeeByType(managers, EmployeeTypes.Manager));
            results.AddRange(GetAllEmployeeByType(seniors, EmployeeTypes.SeniorManager));
            return results;
        }

        private List<IDALEmployeeModel> GetAllEmployeeByType(int count, EmployeeTypes type)
        {
            var results = new List<IDALEmployeeModel>();
            for(var index = 0; index < count; index++)
            {
                results.Add(new BaseEmployee()
                {
                    Id = Guid.NewGuid(),
                    Status = true,
                    Type = (int)type
                });
            }
            return results;
        }

        private IDALParametersAppModel GetDalModel(IBLLParametersAppModel model)
        {
            return new ParametersAppModel()
            {
                MaxDurationCall = model.MaxDurationCall,
                MinDurationCall = model.MinDurationCall,
                IncomingCallsCount = model.IncomingCallsCount,
                ManagerCount = model.ManagerCount,
                OperatorCount = model.OperatorCount,
                SeniorManagerCount = model.SeniorManagerCount
            };
        }
    }
}
