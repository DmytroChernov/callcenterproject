﻿using CCP.DAL.Interfaces.Models;
using System.Collections.Generic;

namespace CCP.DAL.Interfaces
{
    public interface IXmlEmployeeSerializer
    {
        IEnumerable<IDALEmployeeModel> Get();
        void Set(IEnumerable<IDALEmployeeModel> model);
    }
}
