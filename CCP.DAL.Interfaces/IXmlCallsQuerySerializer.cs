﻿using System.Collections.Generic;

namespace CCP.DAL.Interfaces
{
    public interface IXmlCallsQuerySerializer
    {
        IEnumerable<int> Get();
        void Set(IEnumerable<int> model);
    }
}
