﻿namespace CCP.DAL.Interfaces.IModels
{
    public interface IDALParametersAppModel
    {
        int OperatorCount { get; }
        int ManagerCount { get; }
        int SeniorManagerCount { get; }
        int IncomingCallsCount { get; }
        int MinDurationCall { get; }
        int MaxDurationCall { get; }
    }
}
