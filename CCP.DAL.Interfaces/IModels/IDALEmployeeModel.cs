﻿using System;

namespace CCP.DAL.Interfaces.Models
{
    public interface IDALEmployeeModel
    {
        Guid Id { get; }
        int Type { get; }
        bool Status { get; }
    }
}
