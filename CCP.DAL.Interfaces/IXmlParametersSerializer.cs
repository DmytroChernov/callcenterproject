﻿using CCP.DAL.Interfaces.IModels;
using System.Collections.Generic;

namespace CCP.DAL.Interfaces
{
    public interface IXmlParametersSerializer
    {
        IDALParametersAppModel Get();
        void Set(IDALParametersAppModel model);
    }
}
