﻿using CCP.BLL.Interfaces.IModels;
using System.Collections.Generic;

namespace CCP.BLL.Interfaces
{
    public interface IParametersAppService
    {
        void SetInitialParameters(IBLLParametersAppModel model);
        IEnumerable<int> GetCalls();
        IEnumerable<IBLLEmployeeModel> GetEmployees();
    }
}
