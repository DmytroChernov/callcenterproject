﻿using System;

namespace CCP.BLL.Interfaces.IModels
{
    public interface IBLLEmployeeModel
    {
        Guid Id { get; }
        int Type { get; }
        bool Status { get; }
    }
}
