﻿namespace CCP.BLL.Interfaces.IModels
{
    public interface IBLLParametersAppModel
    {
        int OperatorCount { get; }
        int ManagerCount { get; }
        int SeniorManagerCount { get; }
        int IncomingCallsCount { get; }
        int MinDurationCall { get; }
        int MaxDurationCall { get; }
    }
}
